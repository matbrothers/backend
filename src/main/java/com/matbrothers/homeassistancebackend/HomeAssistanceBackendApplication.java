package com.matbrothers.homeassistancebackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HomeAssistanceBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(HomeAssistanceBackendApplication.class, args);
	}

}
