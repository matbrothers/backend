package com.matbrothers.homeassistancebackend.controllers;

import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "util", produces = MediaType.APPLICATION_JSON_VALUE)
public class UtilsController {

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping(value = "/", produces = "application/json")
    public String[] getUtils() {
        return new String[]{"ksiazka 1" , "ksiazka 2"};
    }

    @GetMapping(value = "/{}", produces = "application/json")
    public String getUtil(@PathVariable int id) {
        return "ksiazka " + id;
    }

    @PostMapping(path = "/")
    public String setUtil(@RequestBody Zmienna zm) {
        return zm.nazwa + zm.id;
    }
}

class Zmienna {
    public int id;
    public String nazwa;
}
