package com.matbrothers.homeassistancebackend.model.daos;

public enum ERole {
    ROLE_USER,
    ROLE_MODERATOR,
    ROLE_ADMIN
}
