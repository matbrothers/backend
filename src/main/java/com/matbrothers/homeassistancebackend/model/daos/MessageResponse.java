package com.matbrothers.homeassistancebackend.model.daos;

import com.matbrothers.homeassistancebackend.model.constants.EMessageResponseType;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class MessageResponse {
    Object message;
    EMessageResponseType type;

    public MessageResponse(Object message) {
        this.message = message;
        this.type = EMessageResponseType.MESSAGE;
    }

    public MessageResponse(Object message, EMessageResponseType type) {
        this.message = message;
        this.type = type;
    }
}
