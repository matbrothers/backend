package com.matbrothers.homeassistancebackend.repositories;

import com.matbrothers.homeassistancebackend.model.daos.ERole;
import com.matbrothers.homeassistancebackend.model.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(ERole name);
}
